//
//  SleepyController.swift
//  WatchKitDemo
//
//  Created by Kenta Nakai on 2015/06/26.
//  Copyright (c) 2015年 UROURO. All rights reserved.
//

import WatchKit
import Foundation

class SleepyController: WKInterfaceController {
    
    @IBAction func onRepeatMenuItem(sender: AnyObject) {
        println("onRepeatMenuItem")
    }
    
    @IBAction func onCheckMenuItem(sender: AnyObject) {
        println("onCheckMenuItem")
    }
    
    @IBAction func onAddMenuItem(sender: AnyObject) {
        println("onAddMenuItem")
    }
    
    @IBAction func onBlockMenuItem(sender: AnyObject) {
        println("onBlockMenuItem")
    }
}