//
//  ListController.swift
//  WatchKitDemo
//
//  Created by Kenta Nakai on 2015/06/26.
//  Copyright (c) 2015年 UROURO. All rights reserved.
//

import Foundation
import WatchKit

class ListController: WKInterfaceController {
    
    @IBOutlet weak var table: WKInterfaceTable!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        let weathers: [String] = ["晴れ", "曇り", "雨"]
        self.table.setNumberOfRows(weathers.count, withRowType: "ListRow")
        
        for var i: Int = 0; i < weathers.count; i++ {
            let controller: ListRowController = self.table.rowControllerAtIndex(i) as! ListRowController
            controller.detailLabel.setText(weathers[i])
        }
        
        let temperatures: [Int] = [10, 15, 20, 25, 30, 35, 40]
        self.table.setNumberOfRows(temperatures.count, withRowType: "ListTemperatureRow")
        
        for var i: Int = 0; i < temperatures.count; i++ {
//            let controller: ListTemperatureRowController = self.table.rowControllerAtIndex(i) as! ListTemperatureRowController
//            controller.detailLabel.setText(String(temperatures[i]))
        }
    }
    
    override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
        println("rowIndex=\(rowIndex)")
    }
}