//
//  InterfaceController.swift
//  WatchKitDemo WatchKit Extension
//
//  Created by Kenta Nakai on 2015/06/26.
//  Copyright (c) 2015年 UROURO. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        self.pushControllerWithName("HelloController", context: nil)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
}
