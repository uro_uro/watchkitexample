//
//  ImageController.swift
//  WatchKitDemo
//
//  Created by Kenta Nakai on 2015/06/26.
//  Copyright (c) 2015年 UROURO. All rights reserved.
//

import Foundation
import WatchKit

class ImageController: WKInterfaceController {
    
    @IBOutlet weak var image: WKInterfaceImage!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        self.image.setImage(UIImage(named: "temp01"))
    }
}